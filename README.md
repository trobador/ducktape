# ducktape.sh

This tool is an ugly (and possibly dangerous) hack; proceed with caution!

Requires PatchELF: https://nixos.org/patchelf.html

## Usage example

### After running mix release in your dev environment

`./ducktape.sh _build/prod`

### After copying your release to its final destination

`./patchpath.sh`
