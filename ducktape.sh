#!/usr/bin/env bash

match_files () {
	printf %s "$1" | grep "ELF .\+ $2, .\+ dynamically linked" | sed -n "s/^\(.\+\):\s\+.*$/\1/p"
}

get_deps () {
	printf %s "$1" | tr "\n" "\0" | xargs -0 -n1 ldd | sed -n "s/^.* => \(.\+\) .*$/\1/p" | sort -u
}

get_rpath () {
	dirname "$1" | xargs realpath deps --relative-to | xargs printf "\$ORIGIN/%s"
}

patch_libs () {
	for lib in $1; do
		printf "Patching shared object: %s\n" "$lib"
		patchelf --set-rpath $(get_rpath "$lib") $lib
	done
}

patch_exes () {
	# 127B (+null byte) ought to be enough for any path:
	pad='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
	for exe in $1; do
		printf "Patching executable: %s\n" "$exe"
		patchelf --set-interpreter $pad --set-rpath $(get_rpath "$exe") $exe
	done
}

if [[ ! -x "$(command -v patchelf)" ]]; then
	printf "%s requires patchelf\n" "$0"
	exit 1
fi

cd "$1" || exit 1

if [[ ! -d "lib" || ! -d "rel" ]]; then
	printf "\"%s\" doesn't look like a release directory\n" "$1"
	exit 1
fi

mkdir -p deps

files=$(find . -type f -exec file {} +)
libs=$(match_files "$files" "shared object")
exes=$(match_files "$files" "executable")

printf "Copying dependencies...\n"
get_deps "$(printf "%s\n%s\n" "$libs" "$exes")" | xargs cp -vH -t deps

patch_libs "$libs"
patch_exes "$exes"

cat << EOF > patchpath.sh
#!/usr/bin/env bash

cd "\$(dirname "\$0")"

ld_linux="\$(pwd)/$(find deps -name 'ld-linux*')"
exes='$exes'

IFS=$'\n'
for exe in \$exes; do
	printf "Patching: %s\n" "\$exe"
	printf "%s\x00" "\$ld_linux" | dd conv=notrunc bs=1 of="\$exe" seek=\$(( \$(stat -c %s "\$exe") - 128 ))
	printf "\n"
done

EOF

chmod +x patchpath.sh

